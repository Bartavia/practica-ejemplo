"""
Cree un diagrama de flujo que dado un año A indique si es o no bisiesto. Un año es bisiesto si es múltiplo de 4,
exceptuando los múltiplos de 100, que sólo son bisiestos cuando son múltiplos de 400, por ejemplo el año 1900 no fue bisiesto, pero el año 2000 si lo fue.
"""

v_año = int(input("Ingrese año : "))

if v_año % 4 == 0 and v_año % 100 != 0 or v_año % 400 == 0:
    print("El año : ",v_año,"es bisiesto")
else:
    print("El año",v_año,"no es bisiesto")


