"""
Diseñe un algoritmo que reciba un número entero,
si dicho número es positivo, entonces se debe imprimir la raíz cuadrada del mismo, si el número es negativo se debe imprimir el cuadrado y el cubo.
"""
numero_entero = int(input("Ingresar numero entero : "))
import math
if numero_entero > 0:
    raiz_cuadrada = math.sqrt(numero_entero)
    print("La raiz cuadrada de ",numero_entero,"es : ",raiz_cuadrada)

else:
    numero_entero < 0
    v_el_cuadrado = math.pow(numero_entero,2)
    v_el_cubo = math.pow(numero_entero,3)
    print("El cuadrado de ",numero_entero,"es",v_el_cuadrado,"El cubo de ",numero_entero,"es : ",v_el_cubo)