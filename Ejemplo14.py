"""
Diseñe un algoritmo que reciba como entrada las horas trabajadas y el salario por hora que recibe un trabajador, calcule el salario total y neto sabiendo que la deducción del Seguro Social es del 9%
"""
IMPUESTO_SEGURO_SOCIAL = 0.09

horas_trabajadas = int(input("Ingresar horas trabajadas : "))
salario_hora_trabajador =  int(input("Ingresar el salario por hora : "))

salario_bruto = horas_trabajadas*salario_hora_trabajador
impuestos = salario_bruto*0.09
salario_neto = salario_bruto-impuestos

print("EL salario total es : ",salario_bruto,"El salario neto es : ",salario_neto)
