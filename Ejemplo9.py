#El precio de venta de un automóvil nuevo para el comprador, es la suma total del costo de
#fabricación del vehículo, del porcentaje de ganancia del vendedor y de los impuestos de venta
#establecidos. Supóngase una ganancia del vendedor del 17% sobre el costo de fabricación del
#vehículo y impuesto de ventas del 13% también sobre el costo del vehículo más la ganancia.
#Diseñe un algoritmo que reciba el costo del auto e imprima el precio de venta para el
#consumidor.

Precio_entrada = int(input("Ingresar Valor de una entrada :"))
Cantidad_adquirida = int(input("Ingresar cantidad de entradas adquiridas :"))

if Precio_entrada == 0 or Precio_entrada < 0:
    print("No ingresar precios iguales a cero o menores que cero")

elif Cantidad_adquirida <= 1 or Cantidad_adquirida >4:
    print("No ingresar cantidades de entradas iguales a uno o menores a uno y tampoco mayores a cuatro.")


elif Cantidad_adquirida == 2:
    Cantidad_2adquirida = Cantidad_adquirida*Precio_entrada
    Descuento_2entradas = Cantidad_2adquirida*0.10
    Descuento_Final2 = Cantidad_2adquirida-Descuento_2entradas
    print("El precio final para la compra de dos entradas con el descuento aplicado seria :",Descuento_Final2)

elif Cantidad_adquirida == 3:
    Cantidad_3adquirida = Cantidad_adquirida * Precio_entrada
    Descuento_3entradas = Cantidad_3adquirida * 0.15
    Descuento_Final3 = Cantidad_3adquirida - Descuento_3entradas
    print("El precio final para la compra de tres entradas con el descuento aplicado seria :", Descuento_Final3)

else:
    Cantidad_adquirida == 4
    Cantidad_4adquirida = Cantidad_adquirida * Precio_entrada
    Descuento_4entradas = Cantidad_4adquirida * 0.20
    Descuento_Final4 = Cantidad_4adquirida - Descuento_4entradas
    print("El precio final para la compra de cuatro entradas con el descuento aplicado seria :", Descuento_Final4)
