"""
Para ingresar a un curso de alto nivel, los candidatos deben realizar tres exámenes, A, B, C.
Diseñe un algoritmo que reciba como entrada la edad y las notas de los tres exámenes.
Calcule y despliegue el promedio de notas obtenido y un mensaje indicando si fue aceptado o no en el curso sabiendo que:
– Tener entre 15 y 18 años inclusive y obtener promedio de los exámenes mayor a 90.
 – Tener más de 18 años y que su promedio esté entre 80 y 90 inclusive.
  – Tener menos de 15 años y un promedio mayor o igual a 90,
   o un promedio mayor o igual a 80 pero no debe obtener menos de un 85 en la nota del examen c.
"""
edad = int(input("Introdusca edad : "))
nota_primer_examen = int(input("Introdusca nota de primer examen : "))
nota_segundo_examen = int(input("Introdusca nota de segundo examen : "))
nota_tercer_examen = int(input("Introdusca nota de tercer examen : "))

promedio_notas = (nota_primer_examen+nota_segundo_examen+nota_tercer_examen)/3

if (edad >= 15 and edad <= 18) and (promedio_notas > 90):
    print("Promedio de notas :",promedio_notas,"\n"" Curso aceptado")

elif (edad >= 15 and edad <= 18) and (promedio_notas < 90):
    print("Promedio de notas :", promedio_notas, "\n"" Curso no aceptado")

elif edad > 18 and promedio_notas >= 80:
    print("Promedio de notas :",promedio_notas,"\n"" Curso aceptado")

elif edad > 18 and promedio_notas < 80:
    print("Promedio de notas :", promedio_notas, "\n"" Curso no aceptado")

elif (edad < 15 and promedio_notas >= 90) or edad < 15 and (promedio_notas >= 80 and nota_tercer_examen > 85):
    print("Promedio de notas :",promedio_notas,"\n"" Curso aceptado")

elif (edad < 15 and promedio_notas >= 90) or edad < 15 and (promedio_notas >= 80 and nota_tercer_examen <= 85):
    print("Promedio de notas :", promedio_notas, "\n"" Curso no aceptado")

else:
    edad < 15 and promedio_notas < 90
    print("Promedio de notas :", promedio_notas, "\n"" Curso no aceptado")

